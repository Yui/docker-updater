import { Application, Router, validate, YamlLoader } from "./deps.ts";
import type { Schema } from "./deps.ts";
import { env } from "./src/env.ts";
import { executeRoute, route } from "./src/util.ts";
import { logger } from "./src/logger.ts";

await import("./src/env.ts");

const configBluePrint: Schema = {
  routes: {
    type: "array",
    elementType: {
      type: "object",
      records: {
        entry: {
          type: "object",
          records: {
            name: { type: "string", required: true },
            authorization: { type: "string", required: true },
            command: { type: "string", required: true },
            cwd: { type: "string", required: false },
            shell: { type: "string", required: false },
          },
        },
      },
    },
    required: true,
  },
};

const yoader = new YamlLoader();

const fileExist = (await Deno.stat(env.config_path)).isFile;
if (!fileExist) throw new Error(`${env.config_path} is not a file.`);

const config = (await yoader.parseFile(env.config_path)) as Record<string, unknown>;

logger.info("Validating config");

validate(configBluePrint, config);

const validatedConfig = config as {
  routes: Record<string, route>;
};

const app = new Application();

const router = new Router();

for (const route of Object.values(validatedConfig.routes)) {
  // @ts-ignore idk
  const routeData = route[Object.keys(route)[0]];

  router.get(`/${routeData.name}/:authorization?`, (ctx) => {
    if (routeData.authorization !== ctx.params.authorization) {
      logger.warn(`ROUTE ${routeData.name}: Unauthorized request`);
      ctx.response.status = 401;
    } else {
      logger.debug(`ROUTE ${routeData.name}: OK`);
      ctx.response.status = 200;
      executeRoute(routeData);
    }
  });
}

app.use(router.routes());

app.listen({ port: env.port, secure: false });

logger.info("Listening on port", env.port);
