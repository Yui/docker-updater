import { load } from "../deps.ts";

export const env = await load({
  port: {
    type: Number,
    default: 8080,
  },
  config_path: {
    type: String,
    default: "./config.yml",
  },
});
