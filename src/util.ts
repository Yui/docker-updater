import { logger } from "./logger.ts";

export type route = {
  name: string;
  authorization: string;
  command: string;
  cwd?: string;
  /** Default: /bin/sh
   *
   * Warning: shell needs to support the -c arg
   * Eg: `/bin/sh -c echo test`
   */
  shell?: string;
};

export async function executeRoute(route: route) {
  logger.debug(`EXECUTING ROUTE ${route.name}`);
  try {
    const p = Deno.run({
      cmd: [route.shell ?? "/bin/sh", "-c", route.command],
      cwd: route.cwd,
    });

    const status = await p.status();
    if (!status.success) {
      logger.error("ROUTE ERROR", status.code);
    }
  } catch (err) {
    logger.critical(`ROUTE exec ERROR`, err);
  }
}
