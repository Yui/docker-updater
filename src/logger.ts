import { ConsoleTransport, Logger, colors } from "../deps.ts";

enum Levels {
  debug,
  info,
  success,
  warn,
  error,
  critical,
}

const levelColors = new Map<Levels, (text: string) => string>([
  [Levels.critical, (text: string) => colors.bold(colors.italic(colors.brightRed(text)))],
  [Levels.error, colors.brightRed],
  [Levels.warn, (text: string) => colors.rgb24(text, { r: 255, g: 127, b: 0 })],
  [Levels.debug, colors.brightYellow],
  [Levels.info, colors.brightCyan],
  [Levels.success, colors.green],
]);

const CTransport = new ConsoleTransport({
  levels: Levels,
  logLevel: Levels.debug,
  formatter: (logData) => {
    const date = colors.blue(`[${logData.date.toLocaleDateString()} ${logData.date.toLocaleTimeString()}]`);
    const level = levelColors.get(logData.levelIndex)!(logData.level);
    const fileName = logData.location?.filename
      ? colors.gray(` (${logData.location.filename} L${logData.location.line} C${logData.location.col})`)
      : "";
    return `${date} ${level}${fileName} ${logData.message}`;
  },
});

export const logger = Logger.create({
  levels: Levels,
  transports: [CTransport],
  levelsWithPath: [Levels.critical, Levels.error],
});
