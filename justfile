# Just settings
set dotenv-load := false

# Variables
unstable := ""
denoflags := "--allow-net --allow-env --allow-read --allow-run"
entryfile := "mod.ts"

## Perms
# read - dotenv
# net - obvious
# env - dotenv
# run - obvious
##

# ========== Dev stuff ==================

dev:
    deno run {{denoflags}} {{entryfile}}

# ========== Prod stuff ==================

prod:
    deno run {{denoflags}} --quiet {{entryfile}} --prod

cachedeps:
    deno cache{{unstable}} deps.ts && deno cache{{unstable}} {{entryfile}}
