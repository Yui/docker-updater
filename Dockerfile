ARG denoversion="alpine"
ARG JUST_INSTALL=/bin/just
ARG JUST_RELEASE_TAG="0.10.5"

FROM mcr.microsoft.com/vscode/devcontainers/base:debian-10 as justfetcher

ARG JUST_INSTALL
ARG JUST_RELEASE_TAG

RUN mkdir $JUST_INSTALL
RUN curl --proto '=https' --tlsv1.2 -sSf https://just.systems/install.sh | bash -s -- --tag $JUST_RELEASE_TAG --to $JUST_INSTALL

FROM denoland/deno:${denoversion} as app

ARG JUST_INSTALL

WORKDIR /app

# Copy the source code
COPY *.ts ./
COPY ./src ./src

# Copy makefile
COPY justfile .

RUN chown -R deno:deno /app

# add runtime deps
## Just
COPY --from=justfetcher $JUST_INSTALL $JUST_INSTALL
ENV PATH="$PATH:$JUST_INSTALL"

## Docker cli
RUN apk update && apk add --no-cache docker-cli
### Compose
# https://github.com/docker/compose/issues/3465#issuecomment-730356093
ENV  export EDGE_MAIN http://dl-cdn.alpinelinux.org/alpine/edge/main &&\
          export EDGE_COMMUNITY http://dl-cdn.alpinelinux.org/alpine/edge/community

RUN   apk update &&\
      apk update --repository=$EDGE_MAIN --repository=$EDGE_COMMUNITY &&\
      apk --no-cache add curl make python3 python3-dev py3-pip gcc libc-dev libffi-dev openssl-dev --repository=$EDGE_MAIN --repository=$EDGE_COMMUNITY &&\
      pip3 --no-cache-dir install --upgrade pip &&\
      pip3 --no-cache-dir install docker-compose &&\
      rm -rf /var/cache/apk/ && rm -rf /root/.cache

### Compose End

## Add git so docker can use it to build images

RUN apk --no-cache add git

USER deno

# Cache deno deps
RUN just cachedeps

USER root

# Allow user to access docker
CMD chmod 777 /var/run/docker.sock && su -m deno -c "just prod"