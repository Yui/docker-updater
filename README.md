# Docker updater

- Configure "tasks" in config.yml

- Dockerfile already includes docker cli and docker-compose

- **WARNING**: This approach technically has some security issues. I do not know a better way.

- Example

```bash
docker run -v ${PWD}/config.yml:/app/config.yml -v /var/run/docker.sock:/var/run/docker.sock updaterimage:latest
```
